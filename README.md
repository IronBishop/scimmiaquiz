# ScimmiaQuiz
ScimmiaQuiz (IPA: /ʃ'ʃimmjakwɪz/) is a test project to learn some node.js fu.
First release: [Linux Day 2013, Turin](http://linuxdaytorino.org/2013/).

## Usage
* apt-get install git npm nodejs
** note: minimum recommended nodejs version is 0.10: you may need to install it manually on some architectures, like ARM / Raspbian
* git clone (src)
* cd scimmiaquiz
* npm install
* npm start
* midori http://127.0.0.1:3000/

Modules: [Express.js](http://expressjs.com/), [Jade](http://jade-lang.com/), [Socket.io](http://socket.io/),  [Passport.js](http://passportjs.org/), [Twitter Bootstrap](http://getbootstrap.com/), [Font Awesome](http://fortawesome.github.io), 

Tested and ready to deploy on:

* [RaspberryPi](http://www.raspberrypi.org/)
* [OpenShift](https://openshift.redhat.com/) (no GPIO support... ^_^)

## Abstract
The great interactive speech is back: <em>press the potato</em> and answer the questions! How many SLOC in Debian? Kryptonite is a manual about ...? What's the fastest sheep in the galaxy? We make use of Raspberry Pi, node.js, DD-WRT, vodka ande the unfailing, celebrated biscuits.

## Abstract (it_IT)
Torna a grande richiesta il talk interattivo tra hacking e fooling: <em>schiaccia la patata</em> e rispondi alle domande! Quante SLOC ci sono in Debian? Kryptonite è un manuale di ...? Qual è la nave più veloce della galassia? Ispirato senza ritegno ai blasonati contest della costa ovest: un'oretta di svago tra nozioni tecniche, meno tecniche e decisamente bizzarre. Ricchi premi e cotillons. Con il supporto di: Raspberry Pi, node.js, DD-WRT, vodka e gli immancabili, celeberrimi biscotti.

## Feature request
- [x] Raspberry Pi hosted
- [x] GPIO support
- [x] ask the audience
- [ ] 50/50
- [x] master role
- [x] score/audience charts
- [50%] authentication
- [ ] identicons for logged out users
- [ ] drag and drop user avatar on selected answer
- [ ] i18n, with i18next
- [ ] ...
