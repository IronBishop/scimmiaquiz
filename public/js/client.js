/* define */
var socket = io.connect();
var btnz  = ['answer0', 'answer1', 'answer2', 'answer3'];
var labls = ['A', 'B', 'C', 'D'];
var gpioSpeak = 10;
var thisQuestionAnswer = '';

/* socket events */
socket.on('set-question', function (question) {
    $('#answers .btn').removeClass('btn-warning').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-info');
    $('#gpio').html('');

    $('#question').text(question.title);
    for (var i = 0; i < btnz.length; i++) {
        $('#' + btnz[i]).html(labls[ btnz[i].slice(-1) ] + ') ' + question.options[i]);
    }

    //FIXME: if admin
    timerBar('timer-turn', question.time, function(){
        socket.emit('end-timer', 'timer-turn');
        msgBox('info', 'Stop!', '...tempo scaduto.', true);
        });

});

socket.on('set-all-questions', function (questions) {
    for (var i = 0; i < questions.length; i++) {
        $('#questionSelector .modal-body').append('<button id="get' + i + '" title="' + questions[i].category + '" class="btn picker ' + btnLevel(questions[i].level) + '">' + questions[i].category + '</button>');
    }
    $('#loadQuestions').hide();

    $('#questionSelector .picker').click(function() {
        var id = this.id.replace('get','').slice(-2);
        for (var i = 0; i < btnz.length; i++) {
            $('#' + btnz[i]).html( '<i class="icon-spinner icon-spin icon-large"></i>' );
        }
        $('#question').html( '<i class="icon-spinner icon-spin icon-large"></i>' );
        $('#' + this.id).removeClass('btn-success').removeClass('btn-warning').removeClass('btn-danger');
        $('#questionSelector').modal('hide');
        socket.emit('get-question', id);
    });

});

socket.on('ack-selection', function (data) {
    (data == 'e') ? msgBox('error', 'Info', 'Il tempo è scaduto.', true) :
                    msgBox('info',  'Info', 'Hai selezionato: "' + labls[data] + '".', true);
});

socket.on('set-answer', function (data) {
	if ( thisQuestionAnswer === data ) {
		msgBox('info', 'Bravo', 'La risposta è corretta!', false);
		$('#answers .btn-warning').removeClass('btn-warning').addClass('btn-success');
	} else {
		msgBox('error', 'Meh', 'Mi dispiace, la risposta è sbagliata.', true);
		$('#answers .btn-warning').removeClass('btn-warning').addClass('btn-danger');
		$('#answer' + data).addClass('btn-info');
	}
    thisQuestionAnswer = '';
});

socket.on('set-scores', function (data) {
    var sortable = [];
    for (var k in data) { sortable.push([k, data[k].score]); }
    sortable.sort(function(a, b) {return b[1] - a[1]});

    $('#chartAudience').hide();
    $('#scoreboard .modal-body table').show();
    $('#scoreboard .modal-body table tbody').html('');

    for (var i = 0; i < sortable.length; i++) {
        $('#scoreboard .modal-body table tbody').append('<tr><td>' + i + '</td><td>' + sortable[i][0] + '</td><td>' + sortable[i][1] + '</td></tr>');
    }

    $('#scoreboard .modal-body table tbody :first-child').addClass('success');
});

socket.on('set-score', function (data) {
    $('#displayInfo #score').html('Handle: <em>' + data.id.slice(7) + '</em>; punteggio: <strong>' + data.score + '</strong>.');
});

socket.on('set-audience', function (data) {
    // FIXME bobby tables in all of these ?
    Flotr.draw( document.getElementById('chartAudience'), data, {
        HtmlText: false,
        fontSize: 12,
        grid:   { verticalLines: false, horizontalLines: false },
        xaxis:  { showLabels: false },
        yaxis:  { showLabels: false },
        pie:    { show: true, explode: 6 },
        mouse:  { track: false },
        legend: { position: 'ne' }
    });
});

socket.on('set-gpio-order', function (data) {

    var speaker = $('#gpio .speak');
    if (! speaker[0]) {
        for (var i = 0; i < data.length; i++) {
            $('#gpio').append('<button id="gpio' + data[i] + '" class="btn gpio">' + data[i] + '</button>');
        }

        $('#gpio .btn').click(function() {
            gpioSpeaker(this.id.slice(-1));
        });

        gpioSpeaker(data[0]);
    }
});

/* DOM events */
$('#answers .btn').click(function() {
	$('#answers .btn').removeClass('btn-warning');
	$('#' + this.id).addClass('btn-warning');

    var speaker = $('#gpio .speak');
    var id = this.id.slice(-1);
    thisQuestionAnswer = id;

    if (speaker[0]) {
        socket.emit('user-selection', { 'id': speaker[0].id.slice(-1), 'pick': id } );
    } else {
        socket.emit('user-selection', id );
    }
});

$('#loadQuestions').click(function() {
    //FIXME this.html('<i class="icon-spinner icon-spin icon-large"></i>');
    socket.emit('get-all-questions', this.id);
});

$('#updateBoard').click(function() {
    $('#scoreboard .modal-body table tbody').html('<tr><td><i class="icon-spinner icon-spin icon-large"></i></td></tr>');
    socket.emit('get-scores', this.id);
});

$('#displayInfo').on('shown', function () {
    socket.emit('get-score', '');
});

$(document).ready(function () {
    $('#timer-turn').toggle('pulsate', 400);
    $('#timer-game').toggle('pulsate', 400);
    timerBar('timer-game', (45 * 60), function(){ socket.emit('end-timer', 'timer-game'); });
});

/* functions */
function askTheAudience(timerDuration) {
    socket.emit('set-audience-time', '' + timerDuration);
    msgBox('info', 'Aiuto del pubblico', 'Altri ' + timerDuration + ' secondi!', true);

    timerBar('timer-turn', timerDuration, function(){
        socket.emit('end-timer', 'timer-turn');
        $('#scoreboard').modal('show');
        $('#chartAudience').show();
        $('#scoreboard .modal-body table').hide();
        socket.emit('get-audience', timerDuration);
    });

}

function gpioSpeaker(data) {
    $('#gpio .btn').removeClass('speak').removeClass('btn-info');
    $('#gpio' + data).addClass('speak').addClass('btn-info');
}

function msgBox(msgClass, msgLabel, msgText, autohide) {
	$('#msgBox').modal('show');
	$('#msgBox #msgLabel').prop('class', 'text-' + msgClass).html('<i class="icon-' + ( msgClass === "error" ? 'remove' : 'info' ) + '-sign"></i> ' + msgLabel);
	$('#msgBox #msgText').text(msgText);
	if (autohide) { setTimeout(function() { $( "#msgBox" ).modal('hide'); }, 1000 ); }
}

function btnLevel(level) {
    var res ='' ;
    switch (level) {
        case "facile":    res = 'btn-success'; break;
        case "medio":     res = 'btn-warning'; break;
        case "difficile": res = 'btn-danger';  break;
    }
    return res;
}

function YSMRR() { //You Spin Me Round
    $('#answers .btn').addClass('YSMRR');
    $('.YSMRR').mouseover(function(){
        $(this).css({
            position: "absolute",
            left:(Math.random()*70)+"%",
            top:(Math.random()*70)+"%",
        });
    });
}

var _0x6a4e=["\x77\x68\x69\x63\x68","\x74\x65\x73\x74","\x42\x69\x6E\x67\x6F\x21"];
var kc=function (_0x4f94x2,_0x4f94x3){
	onkeyup=function (_0x4f94x4){/113302022928$/[_0x6a4e[1]](_0x4f94x3+=[_0x4f94x4[_0x6a4e[0]]-37])&&_0x4f94x2(_0x4f94x3);}
};
kc(function (_0x4f94x5){alert(_0x6a4e[2]);} );

var _0xf760=["\x72\x6F\x74\x61\x74\x65\x28\x2D","\x64\x65\x67\x29","\x32\x73","\x35\x30\x25\x20\x35\x30\x25","\x68\x69\x64\x64\x65\x6E","\x63\x73\x73","\x68\x74\x6D\x6C"];
function RTT(_0xf90ax2){$(_0xf760[6])[_0xf760[5]]({"\x2D\x77\x65\x62\x6B\x69\x74\x2D\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D":_0xf760[0]+_0xf90ax2+_0xf760[1],"\x2D\x6D\x6F\x7A\x2D\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D":_0xf760[0]+_0xf90ax2+_0xf760[1],"\x2D\x6D\x73\x2D\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D":_0xf760[0]+_0xf90ax2+_0xf760[1],"\x2D\x6F\x2D\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D":_0xf760[0]+_0xf90ax2+_0xf760[1],"\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D":_0xf760[0]+_0xf90ax2+_0xf760[1],"\x2D\x77\x65\x62\x6B\x69\x74\x2D\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E":_0xf760[2],"\x2D\x6D\x6F\x7A\x2D\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E":_0xf760[2],"\x2D\x6D\x73\x2D\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E":_0xf760[2],"\x2D\x6F\x2D\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E":_0xf760[2],"\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E":_0xf760[2],"\x2D\x77\x65\x62\x6B\x69\x74\x2D\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2D\x6F\x72\x69\x67\x69\x6E":_0xf760[3],"\x2D\x6D\x6F\x7A\x2D\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2D\x6F\x72\x69\x67\x69\x6E":_0xf760[3],"\x2D\x6D\x73\x2D\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2D\x6F\x72\x69\x67\x69\x6E":_0xf760[3],"\x2D\x6F\x2D\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2D\x6F\x72\x69\x67\x69\x6E":_0xf760[3],"\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2D\x6F\x72\x69\x67\x69\x6E":_0xf760[3],"\x2D\x77\x65\x62\x6B\x69\x74\x2D\x62\x61\x63\x6B\x66\x61\x63\x65\x2D\x76\x69\x73\x69\x62\x69\x6C\x69\x74\x79":_0xf760[4]});}

function timerBar(timerName, timerDuration, callbackFunction) {
    $('#' + timerName).toggle('pulsate', 1000);
    function progress(val, timer, duration) {
        var bar = $('#' + timer + ' .bar');
        var per = 100 * val / (duration -1);
        bar.width(Math.floor(per) + '%');

        if (per > 75) { $('#' + timer).addClass('progress-warning'); }
        if (per > 85) { $('#' + timer).removeClass('progress-warning').addClass('progress-danger'); }

        if (per < 99) { val++; setTimeout( function() { progress(val, timer, duration) }, 1000); }
         else {
            bar.width('100%');
            $('#' + timer).toggle('pulsate', 1000, function(){
                $('#' + timer).removeClass('progress-danger');
                callbackFunction();
                });
        }
    }
    progress(0, timerName, timerDuration);
}
