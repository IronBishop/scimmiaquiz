/* define */
var express = require('express')
  , http = require('http')
  , path = require('path')
  , gpio = require('gpio')
  , bcrypt   = require('bcrypt')
  , passport = require('passport')
  , LocalStr = require('passport-local').Strategy
  , passSckt = require("passport.socketio")
  , app  = express()
  , memStore = express.session.MemoryStore
  , sessionStore = new memStore()
  , sessionSecr  = process.env.COOKIE || 'qfLrZ9Y3qfLrZ9Y3qfLrZ9Y3qfLrZ9Y3';

Array.prototype.shuffle = function (){
    var i = this.length, j, temp;
    if ( i == 0 ) return;
    while ( --i ) {
        j = Math.floor( Math.random() * ( i + 1 ) );
        temp = this[i];
        this[i] = this[j];
        this[j] = temp;
    }
};

/* configure */
app.configure(function(){
  app.set('ipaddress', process.env.OPENSHIFT_NODEJS_IP || process.env.IP || '127.0.0.1');
  app.set('port', process.env.OPENSHIFT_NODEJS_PORT  || process.env.PORT || 3000);
  app.set('ADMIN', process.env.ADMIN || '$2a$10$f7Er5z7rrk5h.JM.iX4.W.sIRILURupVJlz81va0VcppANAddWzzu');
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser(sessionSecr));
  app.use(express.session({store: sessionStore}));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use( function(req, res, next){ app.locals.pretty = true; next(); });
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

/* passport */
passport.serializeUser( function(user, done) { done(null, user); });
passport.deserializeUser(function(obj, done) { done(null, obj);  });

passport.use(new LocalStr(
    function(username, password, done) {
        if (username === 'admin' && bcrypt.compareSync(password, app.get('ADMIN'))) {
            console.log('Admin successful login.');
            return done(null, { username: username });
        } else {
            console.log('Wrong username or password.');
            return done(null, false, { message: 'Questi non sono i droni che state cercando.' });
        }
    }
));

app.get('/logout', function(req, res){ req.logout(); res.redirect('/'); });
app.post('/login', passport.authenticate('local', { failureRedirect: '/login', failureFlash: false }), function(req, res) { res.redirect('/admin'); });

app.get('/login', function(req, res){
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<form action="/login" method="post"> \
        <div><label>Username:</label><input type="text" name="username"/><br/></div>\
        <div><label>Password:</label><input type="password" name="password"/></div>\
        <div><input type="submit" value="Submit"/></div></form>'); res.end();});

function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) { return next(); }
	res.redirect('/login');
}

/* project info # FIXME: import from package.json */
var URL = process.env.APP_URL || ( app.get('ipaddress') + ':' + app.get('port') );

var data = {
    project: {
        name: "Scimmia Quiz",
        description: "A test project to learn some node.js fu.",
        author: "Iron Bishop",
        source: "https://github.com/ironbishop/scimmiaquiz",
        URL:    URL,
        license: {
            href: "http://www.tldrlegal.com/license/gnu-affero-general-public-license-v3-%28agpl-3.0%29",
            name: "GNU AGPL"
        }
    }
};

/* listener */
app.get('/',                           function(req,res) { res.render('app', data); });
app.get('/admin', ensureAuthenticated, function(req, res){ res.render('admin', data); });

var server = http.createServer(app).listen(app.get('port'), app.get('ipaddress'), function(){
  console.log('Server listening on ' + app.get('ipaddress') + ':' + app.get('port'));
});

/* socket */
var io = require('socket.io').listen(server);

io.set('authorization', passSckt.authorize({
    cookieParser: express.cookieParser,
    key: 'connect.sid',
    store: sessionStore,
    secret: sessionSecr,
    fail: function(data, accept) { // *optional* callbacks on success or fail
        accept(null, true); // second param takes boolean on whether or not to allow handshake
    },
    success: function(data, accept) {
        accept(null, true);
    }
}));

/* ***
Socket Events:
* connection
* get-all-questions (admin)			// client ask for question list
* set-all-questions (admin)			// push data "question list" to client
* get-question (admin)				// client ask for a specific question
* set-question						// push data "current question" to client
* user-selection					// client answer pick event
* ack-selection                     // ack to client user-selection
* set-answer						// broadcast "correct answer" to client
* set-correct-answer (admin)		// set the correct answer
* get-scores                        // client ask for scoreboard
* set-scores						// push data "score"
* end-timer (admin)                 // end of timer event
* set-audience (admin)              // set additional time for audience
* get-audience (admin)              // client ask for audience answers

+ aggiungere a client: prima schermata: "CIao :) Ti va di giocare? Fai login!
+       Privacy 110%, a fine partita l'applicazione viene spenta e tutti i dati vengono cancellati

*** */
var allQuestions = require( process.env.QUESTIONS || './questions-test.json');
var users        =  {};
var openTurn     = false;
var gpioOrder    = [];
var question     = { title: '' };

io.configure( function() {
    io.set('close timeout', 60*60*2); // 2h time out
});

for (var i = 0; i < allQuestions.length; i++) {
    allQuestions[i].options.push(allQuestions[i].correct);
    allQuestions[i].options.shuffle();
}

/*GPIO */
if (process.env.GPIO) {
    var gpioUsers = {};

    function gpioReserve(id) {
        console.log('GPIO ' + id + ' pressed');
        var already = false;

        for (var i = 0; i < gpioOrder.length; i++) {
            if (gpioOrder[i] === id) { already = true; }
        }

        if (! already) {
            gpioOrder.push(id);
            console.log('GPIO ' + id + ' bespeak');
        }
    }

    gpioUsers.gpio0 = gpio.export( 4, { direction: 'in' }); gpioUsers.gpio0.set(1);
    gpioUsers.gpio1 = gpio.export(17, { direction: 'in' }); gpioUsers.gpio1.set(1);
    gpioUsers.gpio2 = gpio.export(21, { diretcion: 'in' }); gpioUsers.gpio2.set(1);
    gpioUsers.gpio3 = gpio.export(22, { direction: 'in' }); gpioUsers.gpio3.set(1);

    gpioUsers.gpio0.on( 'change', function(val) { if (val) { gpioReserve(0) } } );
    gpioUsers.gpio1.on( 'change', function(val) { if (val) { gpioReserve(1) } } );
    gpioUsers.gpio2.on( 'change', function(val) { if (val) { gpioReserve(2) } } );
    gpioUsers.gpio3.on( 'change', function(val) { if (val) { gpioReserve(3) } } );

    users.gpio0 = { score: 0, currentAnswer: ''};
    users.gpio1 = { score: 0, currentAnswer: ''};
    users.gpio2 = { score: 0, currentAnswer: ''};
    users.gpio3 = { score: 0, currentAnswer: ''};
}

function setCorrectAnswer(data) {
    for (var k in users) {
        if (users.hasOwnProperty(k)) {
            users[k].score += ( ( users[k].currentAnswer === data ) ? 3 : -1 );
            users[k].currentAnswer = '';
        }
    }

    gpioOrder = [];
    io.sockets.emit('set-answer', data);
}

io.sockets.on('connection', function (socket) {
    console.log('User ' + socket.id + ' connected.');
    users[socket.id] = { score: 0, currentAnswer: ''};

    if (question.title) { socket.emit('set-question', question ); }

    socket.on('get-all-questions', function (data) {
        if ( socket.handshake.user && socket.handshake.user.username === 'admin') {
            console.log('Admin requested question list.');
            var questions = [];
            for (var i = 0; i < allQuestions.length; i++) {
                questions[i] = { 'category': allQuestions[i].category, 'title': allQuestions[i].title, 'level': allQuestions[i].level };
            }
            socket.emit('set-all-questions', questions);
        }
    });

    socket.on('get-question', function (data) {
        if ( socket.handshake.user && socket.handshake.user.username === 'admin') {
            data = data.replace(/[^0-9]/g,'').slice(-2);
            console.log('Admin selected question "' + data + '".');
            openTurn = true;
            question = { 'id' : data, 'title': allQuestions[data].title, 'time': allQuestions[data].time, 'options': allQuestions[data].options };
            io.sockets.emit('set-question', question);
        }
    });

    socket.on('user-selection', function (data) {
        if ( socket.handshake.user && socket.handshake.user.username === 'admin') {
            data.id = data.id.replace(/[^0-3]/g,'').slice(-1);
            data.pick = data.pick.replace(/[^0-3]/g,'').slice(-1);
            console.log('Admin set gpio' + data.id + ' selected answer "' + data.pick + '".');
            if (users['gpio' + data.id]) { users['gpio' + data.id].currentAnswer = data.pick; }

             if ( question.options[data.pick] === allQuestions[question.id].correct ) {
                 if (data.id == gpioOrder[0]) { users['gpio' + gpioOrder[0]].score += 2; }
                 setCorrectAnswer(data.pick);
             } else {
                 socket.emit('set-answer', '?');
             }

        } else {
            data = data.replace(/[^0-3]/g,'').slice(-1);

            if (openTurn) {
                console.log('User ' + socket.id + ' selected answer "' + data + '".');
                users[socket.id].currentAnswer = data;
                socket.emit('ack-selection', data);
            } else {
                socket.emit('ack-selection', 'e');
            }
        }
    });

    socket.on('set-correct-answer', function (data) {
        if ( socket.handshake.user && socket.handshake.user.username === 'admin') {
            data = data.replace(/[^0-3]/g,'').slice(-1);
            console.log('Admin set answer "' + data + '" as correct.');

            setCorrectAnswer(data);
        }
    });

    socket.on('get-scores', function (data) {
        if ( socket.handshake.user && socket.handshake.user.username === 'admin') {
            console.log('Admin requested score update.');
            // FIXME: exposing people's session id!!!
            socket.emit('set-scores', users);
        }
    });

    socket.on('get-score', function (data) {
        if (users[socket.id]) { socket.emit('set-score', { 'id': socket.id, 'score': users[socket.id].score}); }
    });

    socket.on('end-timer', function (data) {
        if ( socket.handshake.user && socket.handshake.user.username === 'admin') {
            data = data.replace(/[^abcdefghijklmnopqrstuvwxyz\-]/g,'');
            console.log('Timer ' + data + ' elapsed.');
            if (data === 'timer-turn') {
                openTurn = false;
                socket.emit('set-gpio-order', gpioOrder);
            }
            if (data === 'timer-game') { /* to be defined */ }
        }
    });

    socket.on('set-audience-time', function (data) {
        if ( socket.handshake.user && socket.handshake.user.username === 'admin') {
            data = data.replace(/[^0-9]/g,'').slice(-2);
            console.log('Ask the audience: ' + data + ' more seconds.');
            openTurn = true;
        }
    });

    socket.on('get-audience', function (data) {
        if ( socket.handshake.user && socket.handshake.user.username === 'admin') {
            var pick = [
                { data: [[0, 0]], label: 'Risposta A', color: 'blue' },
                { data: [[0, 0]], label: 'Risposta B', color: 'red'  },
                { data: [[0, 0]], label: 'Risposta C', color: 'purple' },
                { data: [[0, 0]], label: 'Risposta D', color: 'cyan' }
            ];

            for (var k in users) {
                if (users.hasOwnProperty(k) && users[k].currentAnswer !== '') { pick[ users[k].currentAnswer ].data[0][1]++; }
            }
            socket.emit('set-audience', pick);
        }
    });

});
